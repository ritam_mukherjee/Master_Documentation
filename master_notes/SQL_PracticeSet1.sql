--
--1.	Query Emp table.
--2.	Select the employees in department 30.
--3.	List the names, numbers and departments of all clerks.
--4.	Find the department numbers and names of employees of all departments with deptno greater than 20.
--5.	Find employees whose commission is greater than their salaries.
--6.	Find employees whose commission is greater than 60 % of their salaries.
--7.	List name, job and salary of all employees in department 20 who earn more than 2000/-.
--8.	Find all salesmen in department 30 whose salary is greater than 1500/-.
--9.	Find all employees whose designation is either manager or president.
--10.	Find all managers who are not in department 30.
--11.	Find all the details of managers and clerks in dept 10.
--12.	Find the details of all the managers (in any dept) and clerks in dept 20.
--13.	Find the details of all the managers in dept. 10 and all clerks in dept 20 and all employees who are neither managers nor clerks but whose salary is more than or equal to 2000/-.
--14.	Find the names of anyone in dept. 20 who is neither manager nor clerk.
--15.	Find the names of employees who earn between 1200/- and 1400/-.
--16.	Find the employees who are clerks, analysts or salesmen.
--17.	Find the employees who are not clerks, analysts or salesmen.
--18.	Find the employees who do not receive commission.
--19.	Find the different jobs of employees receiving commission.
--20.	Find the employees who do not receive commission or whose commission is less than 100/-.
--21.	If all the employees not receiving commission is entitles to a bonus of Rs. 250/- show the net earnings of all the employees.
--22.	Find all the employees whose total earning is greater than 2000/- .  
--23.	Find all the employees whose name begins or ends with �M� 
--24.	Find all the employees whose names contain the letter �M� in any case.
--25.	Find all the employees whose names are upto 15 character long and have letter �R� as 3rd character of their names.
--26.	Find all the employees who were hired in the month of February (of any year).
--27.	Find all the employees who were hired more than 2 years ago.
--28.	Find the managers hired in the year 2003.
--29.	Display the names of all the employees without any leading �A�.
--30.	Display the names of all the employees without any trailing �R�.
--31.	Show the first 3 and last 3 characters of the names of all the employees.
--32.	Display the names of all the employees  replacing �A� with �a�.
--33.	Show the salary of all the employees , rounding it to the nearest Rs. 1000/-.
--34.	Show the salary of all the employees , ignoring the fraction less than Rs. 1000/-.
--35.	Show the names of all the employees and date on which they completed 3 years of service.
--36.	For each employee, display the no. of days passed since the employee joined the company.
--37.	For each employee, display the no. of months passed since the employee joined the company.
--38.	Display the details of all the employees sorted on the names.
--39.	Display the names of the employees, based on the tenure with the oldest employee coming first.
--40.	Display the names, jobs and salaries of employees, sorting on job and salary.
select * from emp order by job asc,sal desc;
--41.	Display the names, jobs and salaries of employees, sorting on descending order of job and within job sorted on salary.
select * from emp order by job desc,sal asc;
--42.	List the employee names, department names and salary for those employees who have completed 1 year of service.
--43.	List the employee names, department names and salary for those employees who are earning 0 commission or commission is null. Sort  your output in the order of department name.
--44.	List the employee names, department names and hiredate for those employees who have joined in 2003 . Sort  your output in the order of joining date.
--45.	List all the department names along with the names of employees in them , irrespective of the fact whether any employee is there or not.
--46.	List all the department names along with the names of managers in them , irrespective of the fact whether any manager is there or not.
--47.	Find names, job and salaries of all employees and also his immediate boss.
select e.empno as "id", e.ename as "name",e.job as "job",e.sal as "sal",
m.empno as "boss id", m.ename as "boss" from emp e  inner join emp m on e.mgr=m.empno;
--48.	Find the names of those employees who earn more than their boss.
select e.empno as "id", e.ename as "name",e.sal as "sal",
m.empno as "boss id", m.ename as "boss",m.sal as "boss sal" from emp e  
inner join emp m on e.mgr=m.empno where e.sal>=m.sal;
--49.	Find all the employees who are senior to their bosses.
select e.empno as "id", e.ename as "name",e.hiredate as "joining",
m.empno as "boss id", m.ename as "boss",m.hiredate as "boss hiredate" from emp e  
inner join emp m on e.mgr=m.empno where e.HIREDATE<m.HIREDATE;
--50.	Find the names of those employees whose immediate boss is in different department.
select e.empno as "id", e.ename as "name",e.deptno as "department",
m.empno as "boss id", m.ename as "boss",m.deptno as "boss department" from emp e  
inner join emp m on e.mgr=m.empno where e.deptno!=m.deptno;
--51.	List all the employee names along with the names of their bosses, irrespective of the fact whether any employee has boss or not.
--52.	List department number and each distinct pair of employees working in that department.
--53.	Display highest, lowest, sum and average salary of all the employees. Round your result to whole numbers.
--54.	Display highest, lowest, sum and average salary for each job.
--55.	Count the number of bosses without listing them.
--56.	Display the difference between the highest and lowest salary.
--57.	Display department name and the difference between the highest and lowest salary for that department.
--58.	Display department name and average salary for that department. Include only those employees who have joined after 1st July 2001.
--59.	Display the  boss and the salary of  lowest paid employee for him. Don�t include minimum salary below Rs. 1000/-.
--60.	Display department name, location name, no. of people working there and average salary. Round average salary to 2 decimal places.
--61.	Count distinct salary figures and number of employees receiving it.
select DISTINCT e2.sal,
(select count(empno) from emp e1 where e1.sal=e2.sal) 
as "SALARY_HOLDER"from emp e2;
--62.	Find all the department details in which at least one employee is working.
select d.deptno,d.dname,(select count(empno) from emp e where e.deptno=d.deptno
) as "Employee strength"  ,d.loc from dept d where (select count(empno) from emp e where e.deptno=d.deptno
)>1 ;
--63.	Find all, who are bosses of least one employee.
--64.	Find average annual salary of all the employees except analysts.
select round(avg(sal)),job from emp group by job having job!='ANALYST';
--65.	Create unique listing of all the jobs that are in dept. 30. Include location of the dept. in the output.
select  distinct dept.deptno,dname,loc,  job  from dept inner join emp on emp.deptno=dept.deptno where dept.deptno=30;
--66.	List employee name, dept. name, job and location of all employees who work in DALLAS.
select  ename,empno,dept.deptno,dname,loc,job  from emp inner join dept on emp.deptno=dept.deptno where LOC='DALLAS';
--67.	List  employee name and hiredate of all employees who  are hired after  BLAKE.
--68.	List  employee name , hiredate, manager name, manager�s hiredate   of all employees who  are hired before their managers.
--69.	Display the job and the difference between the highest and the lowest salary for each job.
--70.	Display manager and the salary of lowest paid employee of the manager.
--71.	Display dept. name, location, no.of employees in the dept. and average salary of the dept. rounded to 2 decimal places.
--72.	List  employee name and hiredate of all employees who  are  in the same  dept. as BLAKE. Exclude BLAKE.
--73.	Display names and salary of all the employees who report to KING.
--74.	Write a query to display name, dept. no, and salary of any employee whose  is not located at DALLAS  but his/her salary and commission match with the   salary and commission of at least one  employee located in DALLAS.
--75.	Display name , hire date and salary of all the employees who have both salary and commission same as SCOTT. Do not include Scott in the list.
--76.	List employees who earn salary higher than the highest salary of clerks.
--77.	List employees whose  salary is higher than the average salary of employees in department no. 10.
--78.	Display  the names of employees who are earning minimum and maximum salary in one line.
--79.	Find out top 4 salaries of the company. Display their rank as well
--80.	Find out earliest 3 employees who have joined the company. Display their rank as well.
--81.	Print employee name, salary and average salary of his department.
--82.	Display ename, department name and grade of each employee who joined the organization before their boss.
--83.	Display each deprtment name and the first employee who joined that department.
--84.	How much more salary Miller needs to earn to be in King�s grade?
--85.	Display employees who joined in the last month(1st day of last month � Last day of last month). Do not hardcode the month name.
--86.	How much more salary does each person need to earn to go in the next grade?
--87.	List different locations from where employees are reporting to King.
--88.	List different grades of employees working in �DALLAS�
select empno,ename,sal,grade,loc from emp
right outer join salgrade on emp.sal>losal and emp.sal<hisal 
inner join dept on emp.deptno=dept.deptno;
--89.	Display grade 2 employees in Finance department who will complete 3 years in March this year.
--90.	Display employees who are earning salary more than the average salary of employees in the same grade.
select empno,ename,sal,grade from emp e
right outer join salgrade s on e.sal>losal and e.sal<hisal where e.sal>
(select round(avg(sal)) from emp  right outer join salgrade g on emp.sal>losal and emp.sal<g.hisal 
group by g.grade having g.grade=e.grade);

select round(avg(sal)) from emp right outer join salgrade on emp.sal>losal and emp.sal<hisal group by grade having grade=1;
--91.	Display employees who are in that same grade as Miller and do not belong to the place which Miller belongs to.
--92.	How many employees are there between the highest grade of a clerk and the lowest grade of a manager?
--93.	List analysts and clerks who are either staying at Chicago or Boston and in grade 3 and above.
--94.	Display department name, grade, Max salary offered to each grade at each department.
--95.	Who�s earning the best salary in each grade and where do they live?

select ename,empno,job,sal from emp where sal in
(select max(sal) from emp group by job);

--96.	Display the locations where total salary of grade 3 employees is greater than total salary of grade 4 employees.
--97.	Display grade, highest salary in that grade and lowest salary in that grade.
--98.	Display location, highest salary in that location and lowest salary in that location.
--99.	Display the department names where every employee is earning more than 2000.
--100.	Display the dept name,  difference between hisal of highest earning employee in that dept and average salary  for every department.
--
--
--
--101.
select d.deptno, d.dname,(select count(empno) from emp where emp.deptno=d.deptno) as "Strength", d.loc from DEPT d;
--
--
--
