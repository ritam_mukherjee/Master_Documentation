Email Set up Master:
POSTFIX:
--------
1. Postfix is a mail server which by default installed in all mail server:
2. It works almost all vendor but it is default MTA[Mail Transfer Agent] on CentOS and Red hat.


Major mail Actors:
--------------------
mail clients		: compose,read, send
mail servers  		: forward,route store

There are 4 major components:
-----------------------------
1. Mail User Agent: Client user application
Composing and formatting the message for sending
[ex. mail,Pyne, mac mail ]

2. Mail Submission Agent:
Receive Message being sent and determine next hop for delivery.


3. Mail Transfer Agent:
Route the message closure to recipient.Generally crosses administrative bounderies.
---postfix/ sendmail

4. Mail Delivery Agent:
Deliver message to user's mailbox. Copies from server spool into user's mailbox.


Email Routing
-------------
The email routing done by MTA through <user name>@<domain-hostname>.
It first check in to hostname and once that found then goes to individual user's box.

Sender's MTA ask for DNS of receiver's email once thet found deliver, then receiver's MTA find user and do rest}

MTA are on internet listning on SMTP.


Mail Relays:
SMTP server RELAY mail from client to foreign. 
There are mainly two categories of RELAY server available:
    A. Open Relay   :   Without authentication they send mails. mainly SPAM mails uses this RELAY.
    B. Secured RELAY:   They are secured SSAL authenticated server. 

PROTOCOLS   :
-------------------
The protocol which used for mail delivery are: IMAP, POP3, SMTP
    1. SMTP     :   [SIMPLE MAIL TRANSFER PROTOCOL] SMTP protocol listen port 25 and secure SMTP listen port 465, It's not SASL enable.
    2. POP3     :   [POST OFFICE PROTOCOL]SASL enable protocol listen port 110, It's SASL enable
    3. IMAP     :   [INTERNET MESSAGE ACCESS PROTOCOL] and access port 143

	These are expose mail from a mail server to network.
	
SMTP simply sends messeage where as IMAP and POP3 receiving protocol.


Installation:
-------------

1.	Postfix 
    Install postfix using yum:
        yum -y install postfix .

2.	Cyrus:
    Install cyrus packages . (For Mail Authentication).
        yum -y install cyrus-*
        
3. Dovecot:
        yum -y install dovecot



POSTfix works almost for all vendor



Orginal unix smtp server EXIM



HOSTNAME:
----------
-	A Linux server with host name and fully qualified domain name (FQDN)
-	The server should have its entry on DNS (Domain Name System)
the hostname we can check:


	$ hostname
	lcsmdh226.med.ge.com

	$ hostname --all-fqdns
	lcsmdh226 lcsmdh226.med.ge.com lcsmdh226.med.ge.com 

	From other vm we can ping lcsmdh226 
	
3.	Configure the fwd-zone file with the mx record entries. 
4.	Configure the main.cf file present in /etc/postfix 

For configuring the mx record entries and main.cf file, we need the fully qualified domain name (FQDN) which in turn needs a DNS entry for our server. I got stuck here since I had to undergone some formalities to get the DNS entry. You have to contact the DNS team to get a DNS entry for the server. The IT team will provide you the details of the DNS team.


For configuring the mx record entries and main.cf file, we need the fully qualified domain name (FQDN) which in turn needs a DNS entry for our server. I got stuck here since I had to undergone some formalities to get the DNS entry. You have to contact the DNS team to get a DNS entry for the server. The IT team will provide you the details of the DNS team.

You can refer this YouTube video for detailed instructions : https://www.youtube.com/watch?v=AEH7uuTnM3o

To do SMTP server setup whether DNS has installed or not that is mandatory:
we can check by this command:





to check network configuration   :
more /etc/sysconfig/network-scripts/ifcfg-*


To check dns configuration:
more /etc/resolv.conf
or 
cat /etc/resolv.conf


To check about hosts:
 more /etc/hosts



There are 4 major components:
1. Mail User Agent: Client user application
Composing and formatting the message for sending

2. Mail Submission Agent:
Receive Message being sent

3. Mail Transfer Agent:
Route the message closure to recipient
---postfix/ sendmail

4. Mail Delivery Agent:
Deliver message to user's mailbox

Mail Relays


------------------------------------------

sudo yum install postfix -y

systemctl status postfix

sudo su -  : change to root user

yum -y install cyrus-*


yum install mailx

Linx administrator  :    
502715220 
 
[root@lcsmdh226 etc]# postconf -d myhostname
myhostname = [lcsmdh226.med.ge.com:]



[root@lcsmdh226 etc]# sudo netstat -plant : grep :25
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:9200            0.0.0.0:*               LISTEN      22702/java
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      13389/sshd
tcp        0      0 10.177.221.226:40192    3.204.108.221:9042      ESTABLISHED 22702/java
tcp        0      0 10.177.221.226:22       10.177.175.49:49283     ESTABLISHED 22716/sshd: axone [
tcp        0      0 10.177.221.226:40194    3.204.108.221:9042      ESTABLISHED 22702/java
tcp6       0      0 :::2377                 :::*                    LISTEN      28320/dockerd
tcp6       0      0 :::7946                 :::*                    LISTEN      28320/dockerd
tcp6       0      0 :::22                   :::*                    LISTEN      13389/sshd


lcsmdh226.med.ge.com


ip -o -f inet addr show | awk '/scope global/ {print $4}'

10.177.221.226/23
172.17.0.1/16
172.18.0.1/16


10.177.221.226/23,172.17.0.1/16,172.18.0.1/16

mynetworks = 10.177.221.226/23,172.17.0.1/16,172.18.0.1/16

#added for RELAY
smtpd_sasl_auth_enable = yes
smtpd_recipient_restrictions =permit_mynetworks permit_sasl_authenticated_reject_unauth_destination
smtpd_sasl_application_name = smtpd
smtpd_sasl_security_options = noanonymous

https://www.cyberciti.biz/faq/linux-find-domain-hostname/


dnsdomainname:
med.ge.com


lcsmdh226.med.ge.com

check received mails:
mailq      

=================
Try resolving this issue with the following commands:

sudo postmap /etc/postfix/transport_maps
sudo postfix reload
or

newaliases
sudo postfix reload
:q!


vi /var/log/maillog

Jul 12 06:51:13 lcsmdh226 postfix/smtpd[6954]: connect from localhost[127.0.0.1]
Jul 12 06:51:13 lcsmdh226 postfix/smtpd[6954]: warning: non-null host address bits in "10.177.221.226/23", perhaps you should use "10.177.220.0/23" instead
Jul 12 06:51:13 lcsmdh226 postfix/smtpd[6954]: warning: non-null host address bits in "10.177.221.226/23", perhaps you should use "10.177.220.0/23" instead
Jul 12 06:51:13 lcsmdh226 postfix/smtpd[6954]: NOQUEUE: reject: RCPT from localhost[127.0.0.1]: 451 4.3.0 <axone@med.ge.com>: Temporary lookup failure; from=<axone@lcsmdh226.med.ge.com> to=<axone@med.ge.com> proto=ESMTP helo=<lcsmdh226.med.ge.com>

systemctl restart saslauthd


systemctl start firewalld



----------

postconf -e inet_interfaces=all

sudo netstat -plant| grep :25
---------------------------


mail check status  :  sudo cat /var/log/maillog


sudo firewall-cmd --add-service=smtp --permanent
sudo firewall-cmd --reload
axone@lcsmdh226.med.ge.com

from=<ax> to= 


ALIASES
------------

aloases present at /etc/aliases

http://www.postfix.org/SASL_README.html#client_sasl_policy

http://xmodulo.com/enable-user-authentication-postfix-smtp-server-sasl.html

The SSLDB Password authentication
-----------------------------------------------------------
The sasldb plugin:
The sasldb auxprop plugin authenticates SASL clients against credentials that are stored in a Berkeley DB database. The database schema is specific to Cyrus SASL. The database is usually located at /etc/sasldb2.

Note
The sasldb2 file contains passwords in plaintext, and should have read+write access only to user postfix or a group that postfix is member of.

The saslpasswd2 command-line utility creates and maintains the database:

% saslpasswd2 -c -u example.com username
sslpasswd2 -c -u lcsmdh226.med.ge.com axone
Password:
Again (for verification):
This command creates an account username@example.com.

Important
users must specify username@example.com as login name, not username.

Run the following command to reuse the Postfix mydomain parameter value as the login domain:

% saslpasswd2 -c -u `postconf -h mydomain` username
Password:
Again (for verification):
Note
Run saslpasswd2 without any options for further help on how to use the command.

The sasldblistusers2 command lists all existing users in the sasldb database:

% sasldblistusers2
username1@example.com: password1
username2@example.com: password2
Configure libsasl to use sasldb with the following instructions:

/etc/sasl2/smtpd.conf:
    pwcheck_method: auxprop
    auxprop_plugin: sasldb
    mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5 NTLM
Note
In the above example adjust mech_list to the mechanisms that are applicable for your environment.


---------------------------
saslpasswd2 -c -u `postconf -h mydomain` abhi1



-------------------------------------------------------------------------------------------------------------------------------
Procedure to Delete Users from sasldb or Change the Password of sasldb users.

To list the users form saslsb
#sasldblistusers2 
or
#sasldblistusers2 | grep username

To add a new user
# saslpasswd2 -c -u <hostname> username
saslpasswd2 -c -u lcsmdh226.med.ge.com axone

To Delete or disable User form SASLDB
#saslpasswd2 -d username
saslpasswd2 -d axone

To Chance Password
#saslpasswd2  username

To Test SASLAUTHD
#testsaslauthd -u username -p password -s smtp
#testsaslauthd -u username -p password -s pop
#testsaslauthd -u username -p password -s imap

testsaslauthd -u axone -p axone -s smtp